﻿![Alt text](inmetrics.png)

# **Desafio QA - Configuração e instalação**

---

## 1 - Ambiente

Instalar ambiente de desenvolvimento JAVA. Para esta aplicação especifica foi utilizado Eclipse.
Baixar e salvar o ChromeDriver em uma pasta local para utilização do recurso automatico do Google.

---

## 2 - Configuração 

Copiar o caminho onde foi salvo o ChromeDriver do passo anterior e salvar na primeira linha do arquivo "config.properties" do projeto:

webdriver.chrome.driver		= "COPIAR CAMINHO AQUI"

---

## 3 - Rodar

Após as configurações de ambiente e aplicação serem feitas, basta rodar a aplicação utilizando as opções Run ou Debug Start do Eclipse.


