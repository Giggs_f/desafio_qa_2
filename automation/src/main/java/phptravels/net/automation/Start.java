package phptravels.net.automation;



import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.github.javafaker.Faker;

import phptravel.net.configuration.PropertiesConfig;
import phptravels.com.page.objects.Login;
import phptravels.com.page.objects.Menu;
import phptravels.com.page.objects.SupplierNew;
import phptravels.com.page.objects.SuppliersMenu;
import phptravels.net.constants.ConfigData.DATA;
import phptravels.net.factory.WebDriverFactory;
import phptravels.net.helpers.Helper;

public class Start {
	static PropertiesConfig _config = PropertiesConfig.INSTANCE;
	static WebDriver _driver;	

	public static void login(String userName, String password) {
		Login login = PageFactory.initElements(_driver, Login.class);
		login.typeUserName(userName);
		login.typePassowrd(password);
		login.btnLoginClick(_driver);
	}
	
	public static void editSupplier() {
		SuppliersMenu suppMnu = PageFactory.initElements(_driver, SuppliersMenu.class);
		suppMnu.btnEditar(_driver);
	}
	
	public static void addSupplier(Faker faker) {
		Menu mnu = PageFactory.initElements(_driver, Menu.class);
		mnu.mnuAccountsClick(_driver);
		mnu.mnuSuppliersClick(_driver);
		
		SuppliersMenu suppMnu = PageFactory.initElements(_driver, SuppliersMenu.class);
		suppMnu.btnAddClick(_driver);
		
		SupplierNew supplier = PageFactory.initElements(_driver, SupplierNew.class);
		
		supplier.typeFirstName(faker.name().firstName());
		supplier.typeLastName(faker.name().lastName());
		supplier.typeEmail(faker.internet().emailAddress());
		supplier.typePassword(faker.internet().password());
		supplier.typeMobileNumber(faker.phoneNumber().cellPhone());
		supplier.selectCountry("Brazil");
		supplier.typeAddress(faker.address().fullAddress());
		supplier.selectSupplierFor("Hotels");
		supplier.typeName(faker.name().name());
				
		supplier.btnSubmitClick(_driver);		
	}

	public static void startBrowser() {
		WebDriverFactory factory = new WebDriverFactory();
		_driver = factory.getInstance(WebDriverFactory.Browser.CHROME);
		_driver.get(DATA.URL.getValue());
	}

	public static void main(String[] args) {
		try {
			startBrowser();
			login(DATA.USERNAME.getValue(), DATA.PASSWORD.getValue());
			Helper.sleep(2000);
			
			Faker faker = new Faker();
			
			addSupplier(faker);
			Helper.sleep(2000);		
			
			editSupplier();
			Helper.sleep(10000);
			
			_driver.close();
			_driver.quit();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());			
		}
	}
}
