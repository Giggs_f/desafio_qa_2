package phptravels.net.constants;

import phptravel.net.configuration.PropertiesConfig;

public class ConfigData {
	static PropertiesConfig _cp = PropertiesConfig.INSTANCE;

	public enum DATA {		

		URL(
				_cp.getProperty("URL")), 
		USERNAME(
				_cp.getProperty("User")), 
		PASSWORD(
				_cp.getProperty("Password"));

		private String _value;

		DATA(String value) {
			_value = value;
		}

		public String getValue() {
			return _value;
		}
	}
}
