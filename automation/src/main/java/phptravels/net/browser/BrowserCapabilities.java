package phptravels.net.browser;

import java.util.HashMap;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import phptravel.net.configuration.PropertiesConfig;

public class BrowserCapabilities {
	PropertiesConfig _cp = PropertiesConfig.INSTANCE;
	
	public DesiredCapabilities getConfigChrome() {
		System.setProperty("webdriver.chrome.driver", _cp.getProperty("webdriver.chrome.driver"));

		ChromeOptions options = new ChromeOptions();
		options.addArguments("--use-fake-ui-for-media-stream=true");
		options.addArguments("--enable-media-stream");
		options.addArguments("--enable-peer-connection");
		options.addArguments("--disable-web-security chrome://version/");
		options.addArguments("--start-maximized");
		options.addArguments("--disable-extensions");
		options.addArguments("--disable-notifications");
		options.addArguments("--disable-infobars");
//		options.addArguments("--kiosk");

		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("download.prompt_for_download", false);
		chromePrefs.put("upload.prompt_for_upload", false);
		chromePrefs.put("profile.default_content_setting_values.notifications", 2);
		chromePrefs.put("credentials_enable_service", false);
		options.setExperimentalOption("prefs", chromePrefs);
		options.addArguments("--test-type");

		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		desiredCapabilities = DesiredCapabilities.chrome();
		desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, options);

		return desiredCapabilities;
	}
	
}
