package phptravels.com.page.objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SuppliersMenu extends PhpTravels {

	@FindBy(xpath="//*[@id=\"content\"]/div/form/button")
	WebElement btnAdd;
	
	@FindBy(xpath="//*[@id=\"content\"]/div/div[2]/div/div/div[1]/div[2]/table/tbody/tr[1]/td[8]/span/a[1]/i")
	WebElement btnEdit;
	
	public void btnEditar(WebDriver driver) {
		clickOnElement(driver, btnEdit);
	}
	
	public void btnAddClick(WebDriver driver) {
		clickOnElement(driver, btnAdd);
	}
}
