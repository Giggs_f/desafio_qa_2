package phptravels.com.page.objects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import phptravel.net.configuration.PropertiesConfig;

public class PhpTravels {
static PropertiesConfig _cp = PropertiesConfig.INSTANCE;
public static final int DEFAULT_TIMEOUT = 60;
	
	protected void chooseOption(WebDriver driver, WebElement element, String option) {
		waitForElementToBeClickable(driver, element);
		chooseOption(element, option);
	}
	
	protected void chooseOption(WebElement element, String option) {
		Select select = new Select(element);
		select.selectByVisibleText(option);
	}
	
	protected void typeValue(WebDriver driver, WebElement elementText, String value) {
		waitForElement(driver, elementText);
		typeValue(elementText, value);
	}
	
	protected void typeValue(WebElement elementText, String value) {
		elementText.clear();
		sleep(1000);
		elementText.sendKeys(value, Keys.TAB);
		sleep(1000);
	}
	
	protected void typeValueWithEnter(WebElement elementText, String value) {	
		elementText.sendKeys(value, Keys.ENTER);
		sleep(500);
	}
	
	protected void clickOnElement(WebDriver driver, WebElement element) {
		waitForElementToBeClickable(driver, element);
		element.click();
	}
	
	public static void waitForElement(WebDriver driver, WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_TIMEOUT);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	public static void waitForElementToBeClickable(WebDriver driver, WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_TIMEOUT);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public static boolean checkElementIsPresent(WebElement element) {
		boolean isPresent = true;
		try {
			sleep(2000);
			element.isDisplayed();
		} catch (Exception e) {
			isPresent = false;
		}

		return isPresent;
	}
	
	public static void sleep(int milisseconds) {
		try {
			Thread.sleep(milisseconds);
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
		
		}
	}

}
