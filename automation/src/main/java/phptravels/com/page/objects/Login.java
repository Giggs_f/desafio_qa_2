package phptravels.com.page.objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Login extends PhpTravels {
	
	@FindBy(xpath="/html/body/div/form[1]/div[1]/input[1]")
	WebElement inputUserName;
	
	@FindBy(xpath="/html/body/div/form[1]/div[1]/input[2]")
	WebElement inputPassword;
	
	@FindBy(xpath="/html/body/div/form[1]/button")
	WebElement btnLogin;
	
	public void typeUserName(String userName) {
		typeValue(inputUserName, userName);
	}
	
	public void typePassowrd(String password) {
		typeValue(inputPassword, password);
	}
	
	public void btnLoginClick(WebDriver driver) {
		clickOnElement(driver, btnLogin);
	}

}
