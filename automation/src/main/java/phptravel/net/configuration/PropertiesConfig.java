package phptravel.net.configuration;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public enum PropertiesConfig {
	INSTANCE;
	private static final String CONFIG_PATH = "/src/main/resources/properties/config.properties";
	private Properties _properties = new Properties();
	
	public void load() {
		try {
			String path = new File(".").getCanonicalPath();
			
			_properties.load(new FileInputStream(path + CONFIG_PATH));
		} catch (IOException e) {
			
		}
	}

	public String getProperty(String key) {
		if (!isLoaded()) {
			load();
		}
		
		String prop = _properties.getProperty(key);
		
		Pattern p = Pattern.compile(".*(\\$\\{(.*)\\})");
		Matcher m = p.matcher(prop);
		while(m.find()) {
			String replacement = m.group(1);
			String varKey = m.group(2);
			prop = prop.replace(replacement, _properties.getProperty(varKey));
			m = p.matcher(prop);
		}
		
		return prop;
	}
	
	public Properties getProperties() {
		if (!isLoaded()) {
			load();
		}
		
		Properties copy = new Properties();
		copy.putAll(_properties);
		
		return copy;
	}
	
	public boolean isLoaded() {
		return !_properties.isEmpty();
	}
	
	
	public static void main(String[] args) {
		PropertiesConfig cp = PropertiesConfig.INSTANCE;
		String driver = cp.getProperty("webdriver.chrome.driver");		
		cp.load();
		
	}
	

}
